const DBConnection = require('../config/db-connection');

class AbandonedCartModel {
  static async listCustomerAbandonedCart(obj, infoLibrary) {
    const { MessageService, Converters } = obj.privateLibraries;
    try {
      const pool = await DBConnection.connect(infoLibrary.credentials);
      const query = `select * from salesforce.abandonedcart where account_id = '018dda36-0c22-48b3-a11e-c8fc3f5ea534'`;
      const result = await pool.query(query).catch(error => error);
      return MessageService.getSuccess(result.rows, 200);
    } catch (error) {
      return error;
    }
  }
}

module.exports = AbandonedCartModel;
