const { Pool, Client } = require('pg');
const _ = require('lodash');
const Raven = require('raven');

const _connectPostgreSQL = async credentials => {
  return new Pool({
    user: credentials.user,
    host: credentials.host,
    database: credentials.database,
    password: credentials.password,
    port: credentials.port,
    ssl: (credentials.ssl === false) ? false : true,
    max: 20, // max pool
    idleTimeoutMillis: 1000 // after 100 ms
  });
};

function DBConnectionModel() {
  this.instanceByUsers = {};

  this.connect = async (credentials) => {
    return await _connectPostgreSQL(credentials);
  };

  this.notHasInstance = userObject => {
    const email = userObject.login;
    if (!this.instanceByUsers[email]) {
      return true;
    }
    if (!this.instanceByUsers[email].instance) {
      return true;
    }
    return false;
  };

  this.generateConnectionToUser = async (req, userObject, isForceConnection) => {
    try {
      const query = { accountId: userObject.accountId };
      const result = true; //await BusinessGenericModel.findOne(CollectionName.CONNECTIONS_ORGANIZATION, query, {});
      if (!result) { return null; }
      const notHasInstance = this.notHasInstance(userObject);
      if (notHasInstance || isForceConnection) {
        const pool = await this.connect(result);
        if (pool && req.session.passport) {
          req.session.passport.connection = { email: userObject.login };
          req.session.email = userObject.login;
          req.session.save();
          return pool;
        }
        return null;
      } else if (!req.session.passport.connection) {
        req.session.passport.connection = { email: userObject.email };
        req.session.email = userObject.email;
        req.session.save();
        return this.getPool(req);
      }
      return this.getPool(req).instance;
    } catch (e) {
      Raven.captureException(e);
      return null;
    }
  };

  this.getPool = (req) => {
    const email = this.getUser(req).email || this.getUser(req).login;
    return this.instanceByUsers[email];
  };

  this.setPoolToUser = (email, pool) => {
    this.instanceByUsers[email] = { instance: pool };
  };

  this.getUser = req => {
    return req.session.passport.user;
  };

  this.generateConnectionAndSetInstance = async req => {
    const pool = await this.generateConnectionToUser(req, this.getUser(req));
    if (!pool) {
      return {
        status: 500,
        message: 'pool have not created',
        error: null
      };
    }
    const user = this.getUser(req);
    this.setPoolToUser(user.login, pool);
  };

  this.query = async (req, q) => {
    let client = null;
    try {
      if (!this.getPool(req) || this.notHasInstance(this.getUser(req))) {
        await this.generateConnectionAndSetInstance(req);
      }
      const email = this.getUser(req).login;
      const instance = this.getPool(req).instance;
      if (!this.instanceByUsers[email].clientConnected) {
        client = await instance.connect();
        this.instanceByUsers[email].clientConnected = client;
      } else {
        client = this.instanceByUsers[email].clientConnected;
      }
      const res = await client.query(q.queryString, q.queryParamsList).catch(error => { throw Error(error) });
      //client.release();
      return res;
    } catch (e) {
      if (e.message === 'Cannot use a pool after calling end on the pool') {
        return await this.forceReconnect(req, q);
      }
      console.log(e);
      return {
        status: 500,
        message: e.message,
        error: e
      };
    }
  };

  this.forceReconnect = async (req, q) => {
    try {
      await this.generateConnectionAndSetInstance(req);
      const client = await this.getPool(req).instance.connect();
      const res = await client.query(q.queryString, q.queryParamsList).catch(error => { throw Error(error) });
      client.release();
      return res;
    } catch (e) {
      console.log(e);
      return e;
    }
  };

  // Clear only one connection from user connected
  this.clearConnection = email => {
    try {
      if (this.instanceByUsers[email] && this.instanceByUsers[email].instance) {
        this.instanceByUsers[email].instance.end();
      }
      this.instanceByUsers[email] = {};
      return this.instanceByUsers[email];
    } catch (e) {
      console.log(e);
    }
  };

  // Clear and close all connections
  this.closeAllConnections = async () => {
    try {
      const r = [];
      for (let key in this.instanceByUsers) {
        const pool = this.instanceByUsers[key].instance;
        r.push(await pool.end());
      }
      //this.instanceByUsers = {};
      return r;
    } catch (e) { console.log(e); }
  };

  this.saveObject = async (req, data) => {
    try {
      let actionQueryName = ''; //saved , updated, removed
      const tableName = data.tableName;
      const payload = data.payload;
      const method = data.method;
      const keys = _.keys(payload);
      const values = _.map(payload, d => d);
      let columns = '';
      let columnValues = '';

      const finalColumns = _.reduce(keys, (result, key, pos) => {
        if (pos < keys.length-1) {
          columns += `${key}, `;
        } else {
          columns += `${key}`;
        }
        return columns;
      }, 0);

      const finalColumnIndexes = _.reduce(keys, (result, key, pos) => {
        if (pos < keys.length-1) {
          columnValues += `$${pos+1}, `;
        } else {
          columnValues += `$${pos+1}`;
        }
        return columnValues;
      }, 0);

      let queryFinal = {};

      if (!method) {
        return {
          status: 400,
          message: 'attribute method with POST/PUT/DELETE it is required'
        };
      }

      if (method.toUpperCase() === 'POST') {
        actionQueryName  = 'saved';
        queryFinal = {
          queryString: `INSERT INTO salesforce.${tableName}(${finalColumns}) VALUES(${finalColumnIndexes})`,
          queryParamsList: values
        };
      } else if (method.toUpperCase() === 'PUT') {
        actionQueryName  = 'updated';
        let columnsWhereClause = '';
        let columnsSet = '';
        const PKs = _.keys(data.primaryKey);
        let indexSet = 0;
        let indexClause = 0;
        const clauseWhere = _.reduce(data.primaryKey, (result, value, pos) => {
          if (indexClause < PKs.length-1) {
            columnsWhereClause += `${pos} = '${value}' AND `;
          } else {
            columnsWhereClause += `${pos} = '${value}'`;
          }
          indexClause = indexClause + 1;
          return columnsWhereClause;
        }, 0);

        columnsSet = _.reduce(keys, (result, key, pos) => {
          if (indexSet < keys.length-1) {
            columnsSet += `${key} = $${indexSet+1}, `;
          } else {
            columnsSet += `${key} = $${indexSet+1}`;
          }
          indexSet = indexSet + 1;
          return columnsSet;
        }, 0);

        queryFinal = {
          queryString: `UPDATE salesforce.${tableName} SET ${columnsSet} WHERE ${clauseWhere}`,
          queryParamsList: values
        };
      } else if (method.toUpperCase() === 'DELETE') {
        actionQueryName  = 'removed';
        let columnsWhereClause = '';
        const PKvalues = _.filter(data.primaryKey, d => { return d });
        const PKs = _.keys(data.primaryKey);
        let indexClause = 0;
        const clauseWhere = _.reduce(data.primaryKey, (result, value, pos) => {
          if (indexClause < PKs.length-1) {
            columnsWhereClause += `${pos} = '${value}' AND `;
          } else {
            columnsWhereClause += `${pos} = '${value}'`;
          }
          indexClause = indexClause + 1;
          return columnsWhereClause;
        }, 0);

        if (PKvalues.length === 0) {
          return {
            status: 400,
            error: {},
            message: 'it is required a valid primary key attribute'
          };
        }

        queryFinal = {
          queryString: `DELETE FROM salesforce.${tableName} WHERE ${clauseWhere}`,
          queryParamsList: values
        };
      }

      const res = await this.query(req, queryFinal);

      if (res && res.status === 500) {
        return res;
      }
      return {
        status: 200,
        message: `record ${actionQueryName} successfully`
      };
    } catch (e) {
      return { message: 'Error to get pool and try to save object', error: e, status: 500 };
    }
  };
}

const a = new DBConnectionModel();
module.exports = a;
